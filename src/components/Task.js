import React , {Component} from 'react'
import PropTypes from 'prop-types'


class Task extends Component{

    styleCompleted(){
        return {
            fontsize: '10px',
            color: 'red',
            textDecoration: this.props.task.done ? 'line-through' : 'none'
        } 
    }
    render() {
        const {task} = this.props
        return <p style={this.styleCompleted()}>
            {task.id}-
            {task.title}-
            {task.description}
            {task.done}
            <input type="checkbox" text="si"/>
            <input type="checkbox" text="no"/>
            <button style={btnDelete} >
                x
            </button>
        </p>
             
        
    }
}

Task.protoType = {
    task: PropTypes.object.isRequired
}

const btnDelete = {
    background: '#ea20270',
    fontsize: '18px',
    color: '#fff',
    padding: '10px 15px',
    borderRadius: '50%',
    cursor: 'pointer',
}
export default Task